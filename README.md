<div align='center'>
    <a href='https://gitlab.com/vitaly-zdanevich/livejournal-com-black/-/raw/master/livejournal-com-black.user.css' alt='Install with Stylus'>
        <img src='https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat' />
    </a>
</div>


Black night theme

With removed ad, footer.

![screenshot](/screenshot.png)


For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

[On UserStyles.world](https://userstyles.world/style/15094/livejournal-black-without-ad)
